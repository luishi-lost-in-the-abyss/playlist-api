'use strict';
module.exports = (sequelize, DataTypes) => {
  const Track = sequelize.define('Track', {
    album_id: DataTypes.INTEGER,
    name: DataTypes.STRING,
    duration: DataTypes.STRING,
    mp3file: DataTypes.STRING
  }, {});
  Track.associate = function(models) {
    // associations can be defined here

    Track.belongsTo(models.Album,{
      foreignKey: 'album_id',
      target_key: 'id',
     });

     Track.belongsToMany(models.Playlist, {
      through: 'user_playlists',
      as: 'playlists',
      foreignKey: 'track_id',
     
    });

    
  };
  return Track;
};
'use strict';
module.exports = (sequelize, DataTypes) => {
  const Album = sequelize.define('Album', {
    name: DataTypes.STRING,
    artist_id: DataTypes.INTEGER
  }, {});
  Album.associate = function(models) {
    // associations can be defined here
     Album.belongsTo(models.Artist,{
      foreignKey: 'artist_id',
      target_key: 'id',
     });

     Album.hasMany(models.Track,{
      foreignKey: 'album_id',
      as: 'tracks'
    });

  };
  return Album;
};
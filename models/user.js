'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    name: DataTypes.STRING,
    account_id: DataTypes.INTEGER,
    profileimg: DataTypes.STRING
  }, {});
  User.associate = function(models) {
    // associations can be defined here
    User.hasMany(models.Playlist,{
      foreignKey: 'user_id',
      as: 'playlists'
    });


 

  };
  return User;
};
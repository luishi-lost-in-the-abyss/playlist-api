'use strict';
module.exports = (sequelize, DataTypes) => {
  const Artist = sequelize.define('Artist', {
    name: DataTypes.STRING,
    account_id: DataTypes.INTEGER,
    profileimg: DataTypes.STRING
  }, {});
  Artist.associate = function(models) {
    // associations can be defined here



    Artist.belongsTo(models.Account, {
      foreignKey: 'account_id',
      as: 'account'
    });
    
    Artist.hasMany(models.Album,{
      foreignKey: 'artist_id',
      as: 'albums'
    });

  };
  return Artist;
};
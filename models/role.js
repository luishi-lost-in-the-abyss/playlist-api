'use strict';
module.exports = (sequelize, DataTypes) => {
  const Role = sequelize.define('Role', {
    role_description: DataTypes.STRING
  }, {});
  Role.associate = function(models) {
    // associations can be defined here
    Role.hasMany(models.Account,{
      foreignKey: 'role_id',
      as: 'accounts'
    });
  };
  return Role;
};
'use strict';
module.exports = (sequelize, DataTypes) => {
  const Playlist = sequelize.define('Playlist', {
    name: DataTypes.STRING,
    user_id: DataTypes.INTEGER
  }, {});
  Playlist.associate = function(models) {
    // associations can be defined here
    Playlist.belongsTo(models.User,{
      foreignKey: 'user_id',
      target_key: 'id',
     });

     Playlist.belongsToMany(models.Track, {
      through: 'user_playlists',
      as: 'tracks',
      foreignKey: 'playlist_id'
    });
  };
  return Playlist;
};
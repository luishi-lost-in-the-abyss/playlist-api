'use strict';
module.exports = (sequelize, DataTypes) => {
  const Track_log = sequelize.define('Track_log', {
    user_id: DataTypes.INTEGER,
    track_id: DataTypes.INTEGER
  }, {});
  Track_log.associate = function(models) {
    // associations can be defined here


  };
  return Track_log;
};
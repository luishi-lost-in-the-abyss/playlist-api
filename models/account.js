'use strict';
module.exports = (sequelize, DataTypes) => {
  const Account = sequelize.define('Account', {
    role_id: DataTypes.INTEGER,
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    isActive: DataTypes.BOOLEAN
  }, {});
  Account.associate = function(models) {
    // associations can be defined here.
   

    Account.hasOne(models.Artist, {
      foreignKey: 'account_id',
      as: 'artist',
    });

    Account.hasOne(models.User, {
      foreignKey: 'account_id',
      as: 'user',
    });

    Account.belongsTo(models.Role,{
      foreignKey: 'role_id',
      target_key: 'id',
     });

   
  };
  return Account;
};
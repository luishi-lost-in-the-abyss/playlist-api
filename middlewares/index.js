let jwt = require('jsonwebtoken');
const User = require('../models').User;
const Artist = require('../models').Artist;
const multer = require('multer');
const Account = require('../models').Account;


const storageimg = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, './uploads/img/');
  },
  filename: function(req, file, cb) {
    cb(null, new Date().toISOString().replace(/:/g, '-') + file.originalname);
  }
});



const storagemp3 = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, './uploads/mp3/');
  },
  filename: function(req, file, cb) {
    cb(null, new Date().toISOString().replace(/:/g, '-') + file.originalname);
  }
});

const fileFilterimg = (req, file, cb) => {
  // reject a file
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    req.fileValidationError = 'jpeg and png files are only accepted';
    return cb(null, false, new Error('jpeg and png files are only accepted'));
  }
};



const fileFiltermp3 = (req, file, cb) => {

  // reject a file
  console.log(file[0]);

  if (file.mimetype === 'audio/mpeg') {
    cb(null, true);
  } else {
    req.fileValidationError = 'mp3 files are only accepted';
    return cb(null, false, new Error('mp3 files are only accepted'));
  }
};

const uploadimg = multer({
  storage: storageimg,
  
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  
  fileFilter: fileFilterimg
  
});

const uploadmp3 = multer({
  storage: storagemp3,
  fileFilter: fileFiltermp3
  
});


module.exports = {
  checkToken(req, res, next) {
    let token = req.headers['x-access-token'] || req.headers['authorization']; // Express headers are auto converted to lowercase



    if (token) {
      if (token.startsWith('Bearer ')) {
        // Remove Bearer from string
        token = token.slice(7, token.length);
      }

      jwt.verify(token, "secret", async (err, decoded)  => {


        if (err) {
          return res.json({
            success: false,
            message: 'Token is not valid'
          });
        } else {
          const acc = await Account.findOne({where: {id: decoded.id}});
          if(!acc.isActive){
            return res.status(403).json({
              message: 'You are banned'
            })
          }
         
          req.decoded = decoded;
          req.decoded.password = acc.password;
          next();
        }

      });
    } else {
      return res.json({
        success: false,
        message: 'Auth token is not supplied'
      });
    }
  },

  isAdmin(req, res, next) {
    if (req.decoded.role === 3) {
      next();
    } else {
      return res.json({
        success: false,
        message: 'Admin required'
      });
    }
  },


  isUser(req, res, next) {

    if (req.decoded.role === 2) {
      return User
        .findOne({
          where: {
            account_id: req.decoded.id
          }
        })
        .then((user) => {

          req.userdetails = user;
          next();
        })
        .catch((error) => res.status(400).send({message: "Something went wrong"}));
    } else {
      return res.json({
        success: false,
        message: 'User required'
      });
    }


  },

  isArtist(req, res, next) {
    if (req.decoded.role === 4) {
      return Artist
      .findOne({
        where: {
          account_id: req.decoded.id
        }
      })
      .then((artist) => {

        req.artistdetails = artist;
        next();
      })
      .catch((error) => res.status(400).send({message: "Something went wrong"}));

    } else {
      return res.json({
        success: false,
        message: 'Artist required'
      });
    }
  },

  uploadimg,
  uploadmp3

}
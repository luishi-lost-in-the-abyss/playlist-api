const Account = require('../models').Account;

const bcrypt = require('bcrypt');

var jwt = require('jsonwebtoken');
const Joi = require('joi');

module.exports = {
    login(req, res) {

        const schema = Joi.object().keys({
            username: Joi.string().required(),
            password: Joi.string().required()
        })
      

        Joi.validate(req.body, schema, async function (err, value) {
            if (err) {
                res.status(400).json({
                    message: err.name
                });
            } else {
                return Account
            .findOne({
                where: {
                    username: req.body.username
                }
            })
            .then(async (account) => {


                if (!account) {
                    return res.status(404).send({
                        message: "Account Does not Exist!"
                    });

                }

                if (!account.isActive) {
                    return res.status(403).send({
                        message: "Account Banned!"
                    });

                }


                const match = await bcrypt.compare(req.body.password, account.password);
                if (match) {
                    let token = jwt.sign({
                            id: account.id,
                            username: account.username,
                            role: account.role_id,
                            password: account.password
                        },
                        process.env.SECRET_KEY, {
                            expiresIn: '24h' // expires in 24 hours
                        }
                    );
                    res.status(200).json({
                        message: 'Authentication successful!',
                        token: token
                    });
                } else {
                    res.status(400).send({
                        message: "Incorrect Password"
                    });
                }


            }).catch((error) => res.status(400).send(error));

            }
        });


      

    }

}
const Role = require('../models').Role;

module.exports = {
    list(req,res){
        return Role
            .findAll({
               
            })
            .then((roles) => res.status(200).send(roles))
            .catch((error) => res.status(400).send(error));
    },
    
  async add(req,res){

        const exists = await Role.findOne({
            where: {
                role_description: req.body.role_description
            }
        });
        if (exists) {
            return res.status(409).json({
                message: 'Role already exist'
            });
        }

        return Role
            .create({
                role_description: req.body.role_description
            })
            .then((role) => res.status(201).send(role))
            .catch((error) => res.status(400).send(error));

    },
    
    getById(req,res){
        return Role
            .findByPk(req.params.id,{

            })
            .then((role) => {
                if(!role){
                    return res.status(404).send({message: "Role does not exist!"});
                }
                return res.status(200).send(role);

            })
            .catch((error) => res.status(400).send(error));
    },

    
    async update(req,res){

        const exists = await Role.findOne({
            where: {
                role_description: req.body.role_description
            }
        });
        if (exists) {
            return res.status(409).json({
                message: 'Role already exist'
            });
        }

        return Role
            .findByPk(req.params.id,{

            })
            .then((role) => {
                if(!role){
                    return res.status(404).send({message: "Role does not exist!"});
                }
                return role
                    .update({
                        role_description: req.body.role_description
                    })
                    .then((role) => res.status(200).send(role))
                    .catch((error) => res.status(400).send(error));
            })
            .catch((error) => res.status(400).send(error));

    },

    delete(req,res){
        return Role
            .findByPk(req.params.id,{

            })
            .then((role) => {
                if(!role){
                    return res.status(404).send({message: "Role does not exist!"});
                } 
                return role
                    .destroy({
                       
                    })
                    .then((role) => res.status(200).send(role))
                    .catch((error) => res.status(400).send(error));
            })
            .catch((error) => res.status(400).send(error));
    },



    
}
const Artist = require('../models').Artist;
const Album = require('../models').Album;
const Track = require('../models').Track;
const Account = require('../models').Account;
const Role = require('../models').Role;
const bcrypt = require('bcrypt');
const Joi = require('joi');
var mp3Duration = require('mp3-duration');

module.exports = {

    list(req, res) {
        return Artist
            .findAll({

            })
            .then((artist) => res.status(200).send(artist))
            .catch((error) => {
                res.status(400).send(error);
            });

    },

    listwithaccount(req, res) {
        return Artist
            .findAll({
                include: [{
                    model: Account
                }]
            })
            .then((artist) => res.status(200).send(artist))
            .catch((error) => res.status(400).send(error));
    },

    listwithalbumsandtracks(req, res) {
        return Artist
            .findAll({
                include: [{
                    model: Album,
                    as: 'albums',
                    include: [{
                        model: Track,
                        as: 'tracks'
                    }]
                }]
            })
            .then((artist) => res.status(200).send(artist))
            .catch((error) => {
                res.status(400).send(error);
            });
    },


    getById(req, res) {
        return Artist
            .findByPk(req.params.id, {
                include: [{
                    model: Album,
                    as: 'albums',
                    include: [{
                        model: Track,
                        as: 'tracks'
                    }]
                }],
            })
            .then((artist) => {

                if (!artist) {
                    return res.status(404).send({
                        message: "Artist does not exist!"
                    });
                }
                return res.status(200).send(artist);


            })
            .catch((error) => res.status(400).send(error));

    },

    add(req, res) {
        console.log(req.body);
        return Artist
            .create({
                name: req.body.name,
            })
            .then((artist) => res.status(201).send(artist))
            .catch((error) => res.status(400).send(error));
    },

    update(req, res) {
        return Artist
            .findByPk(req.params.id, {
                attributes: ['id']
            })
            .then((artist) => {
                if (!artist) {
                    return res.status(404).send({
                        message: "Artist does not exist!"
                    });
                }
                return artist
                    .update({
                        name: req.body.name
                    })
                    .then((artist) => res.status(200).send(artist))
                    .catch((error) => res.status(400).send(error));

            })
            .catch((error) => res.status(400).send(error));
    },

    delete(req, res) {
        return Artist
            .findByPk(req.params.id, {

            })
            .then((artist) => {
                if (!artist) {
                    res.status(404).send({
                        message: "Artist Does not Exist!"
                    })
                }
                return artist
                    .destroy({

                    }, {
                        include: [{
                            model: Album,
                            as: 'albums'
                        }]
                    })
                    .then((artist) => res.status(200).send(artist))
                    .catch((error) => res.status(400).send(error));
            })
            .catch((error) => res.status(400).send(error));
    },

    addWithAlbums(req, res) {
        return Artist
            .create({
                name: req.body.name,
                albums: req.body.albums
            }, {
                include: [{
                    model: Album,
                    as: 'albums'
                }]
            })
            .then((artist) => res.status(201).send(artist))
            .catch((error) => res.status(400).send(error));

    },

    //start
    getAlbums(req, res) {
        return Album
            .findAll({
                /*
                                include: [{
                                    model: Artist
                                }, ],
                */
                where: {
                    artist_id: req.artistdetails.id
                },
            })
            .then((albums) => res.status(200).json({
                albums: albums
            }))
            .catch((error) => res.status(400).send(error));
    },


    async addAlbumWithTracks(req, res) {

        if(req.fileValidationError) {
            return res.status(415).json({
                message: req.fileValidationError
            });
         }

        let tracks = [];

        const exists = await Album.findOne({
            where: {
                name: req.body.name
            }
        });
        if (exists) {
            return res.status(409).json({
                message: 'Album Name Already Exist'
            });
        }

        for (i = 0; i < req.files.length; i++) {
            let trackduration = await mp3Duration(req.files[i].path, function (err, duration) {
                if (err) return console.log(err.message);
                return duration;
            });
            tracks.push({
                name: req.files[i].originalname,
                duration: trackduration,
                mp3file: req.files[i].path
            });
        }

        req.body.tracks = tracks;

        const schema = Joi.object().keys({
            name: Joi.string().min(6).required(),
            tracks: Joi.array().min(1).required().items(Joi.object({
                name: Joi.string().required(),
                duration: Joi.number().required(),
                mp3file: Joi.string().required(),

            }))
        });
1
        Joi.validate(req.body, schema, async function (err, value) {
            if (err) {
                res.status(400).json({
                    message: err.details[0].message
                });
            } else {

                return Album
                    .create({
                        name: req.body.name,
                        artist_id: req.artistdetails.id,
                        tracks: req.body.tracks
                    }, {
                        include: [{
                            model: Track,
                            as: 'tracks'
                        }]
                    })
                    .then((album) => {
                        return Album
                            .findOne({
                                where: {
                                    id: album.id
                                },
                                include: [{
                                    model: Track,
                                    as: 'tracks'
                                }],
                            }).then((album) => res.status(201).json({
                                message: 'Album '+ album.name + ' created',
                                album: album
                            }))
                            .catch((error) => res.status(400).send(error));
                    })
                    .catch((error) => res.status(400).send(error));
            }
        });

    },

    deleteAlbum(req, res) {


        return Album
            .findOne({
                where: {
                    id: req.params.id,
                    artist_id: req.artistdetails.id
                }
            })
            .then((album) => {
                if (!album) {
                    return res.status(404).send({
                        message: "Album does not exist!"
                    });
                }
                return album
                    .destroy()
                    .then((album) => res.status(200).send({
                        message: 'Album with id ' + req.params.id + ' deleted.'
                    }))
                    .catch((error) => res.status(400).send(error));
            })
            .catch((error) => res.status(400).send(error));
    },


    async updateAlbumname(req, res) {



        const exists = await Album.findOne({
            where: {
                name: req.body.name
            }
        });
        if (exists) {
            return res.status(409).json({
                message: 'Album Name Already Exist'
            });
        }

        

        return Album
        .findOne({
            where: {
                id: req.params.id,
                artist_id: req.artistdetails.id
            }
        })
            .then((album) => {
                if (!album) {
                    return res.status(404).send({
                        message: "Album does not exist!"
                    });
                }
                return album
                    .update({
                        name: req.body.name
                    })
                    .then((album) => res.status(200).json({
                        message: 'Updated Sucessfully',
                        album: album
                    }))
                    .catch((error) => res.status(400).send(error));
            })
            .catch((error) => res.status(400).send(error));
    },

    async addTracktoAlbum(req, res) {
        console.log(req.file);

        let trackduration;

        await mp3Duration(req.file.path, function (err, duration) {
            if (err) return console.log(err.message);
            trackduration = duration;
        });

        return Track
            .create({
                name: req.file.originalname,
                album_id: req.params.id,
                duration: trackduration,
                mp3file: req.file.path
            })
            .then((track) => res.status(200).send(track))
            .catch((error) => res.status(400).send(error));
    },

    async addTrackstoAlbum(req, res) {



        if(req.fileValidationError) {
            return res.status(415).json({
                message: req.fileValidationError
            });
         }
    

        let tracks = [];

        const exists = await Album.findOne({
            where: {
                id: req.params.albumid,
                artist_id: req.artistdetails.id
            }
        });
        if (!exists) {
            return res.status(404).json({
                message: 'Album Does not exist'
            });
        }

        for (i = 0; i < req.files.length; i++) {
            let trackduration = await mp3Duration(req.files[i].path, function (err, duration) {
                if (err) return console.log(err.message);
                return duration;
            });
            tracks.push({
                album_id: req.params.albumid,
                name: req.files[i].originalname,
                duration: trackduration,
                mp3file: req.files[i].path
            });
        }


        const schema = Joi.array().min(1).required().items(Joi.object({
            album_id: Joi.number().required(),
            name: Joi.string().required(),
            duration: Joi.number().required(),
            mp3file: Joi.string().required(),
        }))



        Joi.validate(tracks, schema, function (err, value) {
            if (err) {
                res.status(400).send(err);
            } else {

                return Track
                    .bulkCreate(tracks)
                    .then((tracks) => {

                        return Album
                            .findOne({
                                where: {
                                    id: req.params.albumid
                                },
                                include: [{
                                    model: Track,
                                    as: 'tracks'
                                }],
                            }).then((album) => res.status(201).send({
                                message: 'Tracks added to album '+album.name,
                                album: album
                            }))
                            .catch((error) => res.status(400).send(error));

                    })
                    .catch((error) => res.status(400).send(error));
            }
        });
    },



    listTrackfromAlbum(req, res) {
        return Album
            .findAll({
                where: {
                    id: req.params.id,
                    artist_id: req.artistdetails.id
                },
                include: [{
                    model: Track,
                    as: 'tracks'
                }],

            })
            .then((tracks) => {
                if(tracks.length === 0){
                   return res.status(404).json({
                        message: 'Album does not exist'
                    })
                }
                return res.status(200).send(tracks)
            }
            )
            .catch((error) => res.status(400).send(error));

    },

    updateTrack(req, res) {
        return Track
            .findByPk(req.params.id, {

            })
            .then((track) => {
                if (!track) {
                    return res.status(404).send({
                        message: "Track does not exist!"
                    });
                }
                return track
                    .update({
                        name: req.body.name,
                    })
                    .then((track) => res.status(200).send(track))
                    .catch((error) => res.status(400).send(error));
            })
            .catch((error) => res.status(400).send(error));
    },

    deleteTrack(req, res) {
        return Track
        .findOne({
            where: {
                id: req.params.id,
            },
            include: [{
                model: Album,
                required: true,
                where: {
                    artist_id: req.artistdetails.id
                }
            }]
            
        })
            .then((track) => {
                if (!track) {
                    return res.status(404).send({
                        message: "track does not exist!"
                    });
                }
                return track
                    .destroy()
                    .then((track) => res.status(200).send({
                        message: 'Track with id ' + req.params.id + ' deleted.'
                    }))
                    .catch((error) => res.status(400).send(error));
            })
            .catch((error) => res.status(400).send(error));
    },

    loadProfile(req, res) {
        return Account
            .findOne({
                include: [{
                    model: Role,
                    attributes: ['role_description']

                }, {
                    model: Artist,
                    as: 'artist',
                    where: {
                        id: req.artistdetails.id
                    }
                }],
            })
            .then((artist) => res.status(200).send(artist))
            .catch((error) => {
                res.status(400).send(error);
            });

    },

    editProfile(req, res) {
        
        if(req.fileValidationError) {
            return res.status(415).json({
                message: req.fileValidationError
            });
         }


        return Artist
            .findByPk(req.artistdetails.id, {

            })
            .then((artist) => {
                return artist
                    .update({
                        name: req.body.name,
                        profileimg: req.file.path,
                    })
                    .then((artist) => {
                        return Account
                            .findOne({
                                include: [{
                                    model: Role,
                                    attributes: ['role_description']

                                }, {
                                    model: Artist,
                                    as: 'artist',
                                    where: {
                                        id: artist.id
                                    }
                                }],
                            }).then((artist) => res.status(200).json({
                                message: 'Profile updated successfully.',
                                account:artist
                            }))
                            .catch((error) => res.status(400).send(error));

                    })
                    .catch((error) => res.status(400).send(error));
            })
            .catch((error) => {
                res.status(400).send(error);
            });
    }








}
const Track = require('../models').Track;
const user_playlist = require('../models').user_playlist;
const Playlist = require('../models').Playlist;
const Album = require('../models').Album;
const Artist = require('../models').Artist;
module.exports = {

    list(req, res) {
        return Track
            .findAll({

            })
            .then((tracks) => res.status(200).send(tracks))
            .catch((error) => res.status(400).send(error));
    },

    listfromalbum(req,res){
        return Track
        .findAll({
            include: [{
                model: Album,
                include: [{
                    model: Artist
                }]
                
            }],
            where: {
                album_id: req.params.albumid
            },
        })
        .then((tracks) => 
        {

            if(tracks.length === 0){
                return res.status(404).json({message: 'Album does not exist'})
            }
           return res.status(200).send(tracks)
        })
       
        .catch((error) => res.status(400).send(error));
    },


    getById(req, res) {
        return Track
            .findByPk(req.params.id, {

            })
            .then((track) => {
                if (!track) {
                    return res.status(404).send({
                        message: "Track does not exist!"
                    });
                }
                return res.status(200).send(track);

            })
            .catch((error) => res.status(400).send(error));
    },

    add(req, res) {
        return Track
            .create({
                name: req.body.name,
                album_id: req.body.artist_id,
                duration: req.body.duration
            })
            .then((track) => res.status(200).send(track))
            .catch((error) => res.status(400).send(error));
    },

    update(req, res) {
        return Track
            .findByPk(req.params.id, {

            })
            .then((track) => {
                if (!track) {
                    return res.status(404).send({
                        message: "Track does not exist!"
                    });
                }
                return track
                    .update({
                        name: req.body.name,
                        album_id: req.body.album_id,
                        duration: req.body.duration
                    })
                    .then((track) => res.status(200).send(track))
                    .catch((error) => res.status(400).send(error));
            })
            .catch((error) => res.status(400).send(error));
    },
    delete(req, res) {
        return Track
            .findByPk(req.params.id, {

            })
            .then((track) => {
                if (!track) {
                    return res.status(404).send({
                        message: "track does not exist!"
                    });
                }
                return track
                    .destroy({ truncate: { cascade: true } })
                    .then((track) => res.status(200).send({
                        message: 'Track with id '+  req.params.id+ ' deleted.'
                    }))
                    .catch((error) => res.status(400).send(error));
            })
            .catch((error) => res.status(400).send(error));
    },

    addtoplaylist(req, res) {
        return Playlist
            .findByPk(req.params.id, {

            })
            .then((playlist) => {
                if (!playlist) {
                    return res.status(404).send({
                        message: 'Playlist Not Found',
                    });
                }
                Track.findByPk(req.body.track_id).then((track) => {
                    if (!track) {
                        return res.status(404).send({
                            message: 'Track Not Found',
                        });
                    }
                    playlist.addTrack(track);
                    
                    return res.status(200).send(playlist);
                })
            })
            .catch((error) => res.status(400).send(error));

    },

    


    removetoplaylist(req, res) {
        return Playlist
            .findByPk(req.params.id, {

            })
            .then((playlist) => {
                if (!playlist) {
                    return res.status(404).send({
                        message: 'Playlist Not Found',
                    });
                }
                Track.findByPk(req.body.track_id).then((track) => {
                    if (!track) {
                        return res.status(404).send({
                            message: 'Track Not Found',
                        });
                    }
                    playlist.removeTrack(track);
                    
                    return res.status(200).send(playlist);
                })
            })
            .catch((error) => res.status(400).send(error));

    },

    listfromplaylist(req,res) {
        return Playlist
        .findAll({
            where: {
                id: req.params.id
            },

            include: [{
              model:Track, as: 'tracks',
            
              through: {
                attributes: ['playlist_id', 'track_id'],
              }
              }]
        })
        .then((tracks) => res.status(200).send(tracks))
        .catch((error) => res.status(400).send(error));
    },

    



}
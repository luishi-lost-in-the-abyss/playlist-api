const Account = require('../models').Account;
const Role = require('../models').Role;
const Playlist = require('../models').Playlist;
const bcrypt = require('bcrypt');
const Artist = require('../models').Artist;
const User = require('../models').User;
const Joi = require('joi');
const Album = require('../models').Album;
const Track = require('../models').Track;

module.exports = {
    addartist(req, res) {

        const schema = Joi.object().keys({
            name: Joi.string().required(),
            username: Joi.string().min(6).required(),
            password: Joi.string().min(6).required()
        })


        Joi.validate(req.body, schema, async function (err, value) {
            if (err) {
                res.status(400).json({
                    message: err.details[0].message
                });
            } else {



                const exists = await Account.findOne({
                    where: {
                        username: req.body.username
                    }
                });
                if (exists) {
                    return res.status(409).json({
                        message: 'Username Taken'
                    });
                }



                const hashedPassword = await new Promise((resolve, reject) => {
                    bcrypt.hash(req.body.password, 5, function (err, hash) {
                        if (err) reject(err)
                        resolve(hash)
                    });
                })

                return Account
                    .create({
                        role_id: 4,
                        username: req.body.username,
                        password: hashedPassword,
                        artist: {
                            name: req.body.name
                        }
                    }, {
                        include: [{
                            model: Artist,
                            as: 'artist'
                        }]
                    })
                    .then((account) => res.status(201).json({
                        message: 'Artist account created',
                        account: account

                    }))
                    .catch((error) => res.status(400).send(error));
            }
        });
    },

    adduser(req, res) {


        const schema = Joi.object().keys({
            name: Joi.string().min(6).required(),
            username: Joi.string().min(6).required(),
            password: Joi.string().min(6).required()
        })



        Joi.validate(req.body, schema, async function (err, value) {
            if (err) {
                res.status(400).json({
                    message: err.name
                });
            } else {


                const exists = await Account.findOne({
                    where: {
                        username: req.body.username
                    }
                });
                if (exists) {
                    return res.status(409).json({
                        message: 'Username Taken'
                    });
                }

                const hashedPassword = await new Promise((resolve, reject) => {
                    bcrypt.hash(req.body.password, 5, function (err, hash) {
                        if (err) reject(err)
                        resolve(hash)
                    });
                })
                return Account
                    .create({
                        role_id: 2,
                        username: req.body.username,
                        password: hashedPassword,
                        user: {
                            name: req.body.name
                        }
                    }, {
                        include: [{
                            model: User,
                            as: 'user'
                        }]
                    })
                    .then((account) => res.status(201).json({
                        message: 'User account created',
                        account: account
                    }))
                    .catch((error) => res.status(400).send(error));
            }
        });

    },

    async addadmin(req, res) {
        const hashedPassword = await new Promise((resolve, reject) => {
            bcrypt.hash(req.body.password, 5, function (err, hash) {
                if (err) reject(err)
                resolve(hash)
            });
        })


        const exists = await Account.findOne({
            where: {
                username: req.body.username
            }
        });
        if (exists) {
            return res.status(409).json({
                message: 'Username Taken'
            });
        }

        return Account
            .create({
                role_id: 3,
                username: req.body.username,
                password: hashedPassword,

            })
            .then((account) => res.status(201).json({
                message: 'Account created',
                account: account

            }))
            .catch((error) => res.status(400).send(error));
    },

    list(req, res) {
        return Account
            .findAll({
                    include: [{
                        model: Role,
                        attributes: ['role_description']
                    }]

                },

            )
            .then((account) => res.status(200).json({
                accounts: account
            }))
            .catch((error) => res.status(400).send(error));
    },

    listuserwithplaylist(req, res) {
        return Account
            .findAll({
                where: {
                    role_id: 2
                },

                include: [{
                    model: Role,
                    attributes: ['role_description']

                }, {
                    model: User,
                    as: 'user',
                    include: [{
                        model: Playlist,
                        as: 'playlists'
                    }]
                }],

            })
            .then((account) => res.status(200).send(account))
            .catch((error) => res.status(400).send(error));
    },


    listartistfull(req, res) {
        return Account
            .findAll({
                where: {
                    role_id: 4
                },

                include: [{
                    model: Role,
                    attributes: ['role_description']

                }, {
                    model: Artist,
                    as: 'artist',
                    include: [{
                        model: Album,
                        as: 'albums',
                        include: [{
                            model: Track,
                            as: 'tracks'
                        }]
                    }]
                }],

            })
            .then((account) => res.status(200).send(account))
            .catch((error) => res.status(400).send(error));
    },

    delete(req, res) {
        return Account
            .findByPk(req.params.id, {

            })
            .then((account) => {
                if (!account) {
                    res.status(404).send({
                        message: "Account Does not Exist!"
                    })
                }
                return account
                    .destroy({

                    })
                    .then((account) => res.status(200).send(account))
                    .catch((error) => res.status(400).send(error));
            })
            .catch((error) => res.status(400).send(error));
    },

    banAccount(req, res) {
        return Account
            .findByPk(req.params.id, {

            })
            .then((account) => {
                if (!account) {
                    return res.status(404).send({
                        message: "Account does not exist!"
                    });
                }
                return account
                    .update({
                        isActive: false
                    })
                    .then((account) => res.status(200).send({
                        message: 'Account with id ' + req.params.id + ' banned.',
                        account: account
                    }))
                    .catch((error) => res.status(400).send(error));
            })
            .catch((error) => res.status(400).send(error));
    },

    unbanAccount(req, res) {
        return Account
            .findByPk(req.params.id, {

            })
            .then((account) => {
                if (!account) {
                    return res.status(404).send({
                        message: "Account does not exist!"
                    });
                }
                return account
                    .update({
                        isActive: true
                    })
                    .then((account) => res.status(200).send({
                        message: 'Account with id ' + req.params.id + ' unbanned.',
                        account: account
                    }))
                    .catch((error) => res.status(400).send(error));
            })
            .catch((error) => res.status(400).send(error));
    },

    async changepass(req, res) {

        const match = await bcrypt.compare(req.body.oldpassword, req.decoded.password);

        if (match) {

            const hashedPassword = await new Promise((resolve, reject) => {
                bcrypt.hash(req.body.newpassword, 5, function (err, hash) {
                    if (err) reject(err)
                    resolve(hash)
                });
            })


            return Account
                .findByPk(req.decoded.id, {})
                .then((account) => {
                    return account
                        .update({
                            password: hashedPassword

                        })
                        .then((account) => {

                            req.decoded = account

                            res.status(200).send({
                                message: 'Password changed.'
                            })

                        })
                        .catch((error) => res.status(400).send(error));
                })
                .catch((error) => {
                    res.status(400).send(error);
                });


        } else {

            res.status(400).send({
                message: 'Incorrect Password!'
            })
        }


    }


}
const Playlist = require('../models').Playlist;

module.exports = {

    add(req, res) {
        return Playlist
            .create({
                name: req.body.name,
                user_id: req.body.user_id
            })
            .then((playlist) => res.status(200).send(playlist))
            .catch((error) => res.status(400).send(error));
    }




}
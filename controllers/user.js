const User = require('../models').User;
const Playlist = require('../models').Playlist;
const Account = require('../models').Account;
const Track = require('../models').Track;
const Album = require('../models').Album;
const Artist = require('../models').Artist;
const track_log = require('../models').Track_log;
const Role = require('../models').Role;

module.exports = {

    loaduserplaylists(req, res) {
        return Playlist
            .findAll({
                where: {
                    user_id: req.userdetails.id
                }
            })
            .then((playlists) => res.status(200).json({playlists:playlists}))
            .catch((error) => res.status(400).send(error));
    },

    loadArtists(req, res) {
        return Artist
            .findAll({
                include: [{
                    model: Account,
                    as: 'account',
                    attributes: [],
                    where: {
                        isActive: true
                    }
                }],

            })
            .then((artists) => res.status(200).json({artists:artists}))
            .catch((error) => res.status(400).send(error));
    },

    loadAlbums(req, res) {
        return Album
            .findAll({
                include: [{
                    model: Artist,
                    required: true,
                    include: [{
                        model: Account,
                        as: 'account',
                        attributes: [],
                        where: {
                            isActive: true
                        }
                    }]
                }],
            })
            .then((albums) => res.status(200).json({albums:albums}))
            .catch((errors) => res.status(400).send(errors));
    },


    loadTracks(req, res) {
        return Track
            .findAll({
                include: [{
                    model: Album,
                    required: true,
                    include: [{
                        model: Artist,
                        required: true,
                        include: [{
                            model: Account,
                            as: 'account',
                            attributes: [],
                            where: {
                                isActive: true
                            }
                        }]

                    }]
                }]

            })
            .then((tracks) => res.status(200).json({tracks:tracks}))
            .catch((error) => res.status(400).send(error));
    },

    loadtracksfromplaylist(req, res) {


        return Playlist
            .findAll({
                where: {
                    id: req.params.playlistid,
                    user_id: req.userdetails.id
                },
                include: [{
                    model: Track,
                    as: 'tracks',
                    through: {
                        attributes: ['playlist_id', 'track_id'],
                    },
                    include: [{
                        model: Album,
                        required: true,
                        include: [{
                            model: Artist,
                            required: true,
                            include: [{
                                model: Account,
                                as: 'account',
                                attributes: [],
                                where: {
                                    isActive: true
                                }
                            }]

                        }]
                    }]
                }]
            })
            .then((tracks) => {
                if(tracks.length === 0){
                   return res.status(404).json({
                        message: 'Playlist Not Found'
                    })
                }

                return res.status(200).send(tracks)
            })
            .catch((error) => res.status(400).send(error));
    },

    async createplaylist(req, res) {



        const exists = await Playlist.findOne({
            where: {
                name: req.body.name,
                user_id: req.userdetails.id
            }
        });
        if (exists) {
            return res.status(409).json({
                message: 'Playlist Name Already Exist'
            });
        }

        return Playlist
            .create({
                name: req.body.name,
                user_id: req.userdetails.id
            })
            .then((playlist) => res.status(201).json({playlist:playlist}))
            .catch((error) => res.status(400).send(error));
    },

    deleteplaylist(req, res) {
        return Playlist
        .findOne({
            where: {
                id: req.params.id,
                user_id: req.userdetails.id
            }
        })
            .then((playlist) => {
                if (!playlist) {
                    return res.status(404).send({
                        message: "Playlist Does not Exist!"
                    })
                }
                return playlist
                    .destroy({})
                    .then((playlist) => res.status(200).send({
                        message: 'Playlist with id ' + req.params.id + ' deleted.'
                    }))
                    .catch((error) => res.status(400).send(error));
            })
            .catch((error) => res.status(400).send(error));
    },

    async updateplaylist(req, res) {

        const exists = await Playlist.findOne({
            where: {
                name: req.body.name,
                user_id: req.userdetails.id
            }
        });
        if (exists) {
            return res.status(409).json({
                message: 'Playlist Name Already Exist'
            });
        }

        return Playlist
        .findOne({
            where: {
                id: req.params.id,
                user_id: req.userdetails.id
            }
        })
            .then((playlist) => {
                console.log(playlist);
                if (!playlist) {
                    return res.status(404).json({
                        message: "Playlist does not exist!"
                    });
                }
                return playlist
                    .update({
                        name: req.body.name
                    })
                    .then((playlist) => res.status(200).json({
                        message: 'Updated Sucessfully',
                        playlist: playlist
                    }))
                    .catch((error) => res.status(400).send(error));

            })
            .catch((error) => res.status(400).send(error));
    },

    addtoplaylist(req, res) {
        return Playlist
        .findOne({
            where: {
                id: req.params.playlistid,
                user_id: req.userdetails.id
            }
        })
            .then((playlist) => {
                if (!playlist) {
                    return res.status(404).send({
                        message: 'Playlist Not Found',
                    });
                }
                Track.findByPk(req.body.track_id).then((track) => {
                    if (!track) {
                        return res.status(404).send({
                            message: 'Track Not Found',
                        });
                    }
                    playlist.addTrack(track)
                        .then((track) => {
                            return Playlist
                                .findAll({
                                    where: {
                                        id: req.params.playlistid
                                    },
                                    include: [{
                                        model: Track,
                                        as: 'tracks',
                                        through: {
                                            attributes: ['playlist_id', 'track_id'],
                                        },
                                        include: [{
                                            model: Album,
                                            include: [{
                                                model: Artist
                                            }]
                                        }]
                                    }]
                                }).then((playlist) => res.status(200).send(playlist))

                        });


                })
            })
            .catch((error) => res.status(400).send(error));

    },

    removetoplaylist(req, res) {
        return Playlist
        .findOne({
            where: {
                id: req.params.playlistid,
                user_id: req.userdetails.id
            }
        })
            .then((playlist) => {
                if (!playlist) {
                    return res.status(404).send({
                        message: 'Playlist Not Found',
                    });
                }
                Track.findByPk(req.body.track_id).then((track) => {
                    if (!track) {
                        return res.status(404).send({
                            message: 'Track Not Found',
                        });
                    }
                    playlist.removeTrack(track)
                        .then((track) => {
                            return Playlist
                                .findAll({
                                    where: {
                                        id: req.params.playlistid
                                    },
                                    include: [{
                                        model: Track,
                                        as: 'tracks',
                                        through: {
                                            attributes: ['playlist_id', 'track_id'],
                                        },
                                        include: [{
                                            model: Album,
                                            include: [{
                                                model: Artist
                                            }]
                                        }]
                                    }]
                                }).then((playlist) => res.status(200).send(playlist))

                        });;

                })
            })
            .catch((error) => res.status(400).send(error));

    },


    loadalbumsbyartist(req, res) {
        return Album
            .findAll({
                include: [{
                    model: Artist,
                }],
                where: {
                    artist_id: req.params.artistid
                }
            })
            .then((albums) => {
                if(albums.length === 0){
                    return res.status(404).json({
                        message: 'Artist does not exist'
                    })
                }

                return res.status(200).json({albums:albums})

            }) 
            .catch((error) => res.status(400).send(error));
    },
    loadtracksbyalbum(req, res) {
        return Track
            .findAll({
                include: [{
                    model: Album,
                    include: [{
                        model: Artist,
                    }],
                }],
                where: {
                    album_id: req.params.albumid
                }
            })
            .then((tracks) => {
                if(tracks.length === 0){
                    return res.status(404).json({
                        message: 'Album does not exist'
                    })
                }
                return res.status(200).json({tracks:tracks});
            })
            .catch((error) => res.status(400).send(error));
    },

    loadProfile(req, res) {
        return Account
            .findOne({
                include: [{
                    model: Role,
                    attributes: ['role_description']

                }, {
                    model: User,
                    as: 'user',
                    where: {
                        id: req.userdetails.id
                    }
                }],
            })
            .then((user) => res.status(200).send(user))
            .catch((error) => {
                res.status(400).send(error);
            });

    },
    editProfile(req, res) {


        if(req.fileValidationError) {
            return res.status(415).json({
                message: req.fileValidationError
            });
         }

        return User
            .findByPk(req.userdetails.id, {

            })
            .then((user) => {
                return user
                    .update({
                        name: req.body.name,
                        profileimg: req.file.path,
                    })
                    .then((user) => {
                        return Account
                            .findOne({
                                include: [{
                                    model: Role,
                                    attributes: ['role_description']

                                }, {
                                    model: User,
                                    as: 'user',
                                    where: {
                                        id: user.id
                                    }
                                }],
                            })
                            .then((user) => res.status(200).json({
                                message: 'Profile updated successfully.',
                                account:user
                            }))
                            .catch((error) => res.status(400).send(error));

                    })
                    .catch((error) => res.status(400).send(error));
            })
            .catch((error) => {
                res.status(400).send(error);
            });
    }



}
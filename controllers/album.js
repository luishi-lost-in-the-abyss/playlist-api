const Album = require('../models').Album;
const Track = require('../models').Track;
const Artist = require('../models').Artist;
module.exports = {

    list(req,res){
        return Album
            .findAll({
                include: [{
                    model: Artist
                }]
            })
            .then((albums) => res.status(200).send(albums))
            .catch((error) => res.status(400).send(error));
    },

    listbyartist(req,res){
        return Album
        .findAll({
            include: [{
                model: Artist
            }],
            where: {
                artist_id: req.params.artistid
            }
        })
        .then((albums) => 
        {
            if(albums.length===0){
                return res.status(404).json({
                    message: 'Artist does not exist'
                })
            }

            return res.status(200).send(albums)
        }

      
        
        
        )
        .catch((error) => res.status(400).send(error));
    },

    getById(req,res){
        return Album
            .findByPk(req.params.id,{
                include: [{
                    model: Artist
                }]
            })
            .then((album) => {
                if(!album){
                    return res.status(404).send({message: "Album does not exist!"});
                }
                return res.status(200).send(album);

            })
            .catch((error) => res.status(400).send(error));
    },

    add(req,res){
        return Album
            .create({
                name: req.body.name,
                artist_id: req.body.artist_id
            })
            .then((album) => res.status(200).send(album))
            .catch((error) => res.status(400).send(error));
    },

    update(req,res){
        return Album
            .findByPk(req.params.id,{

            })
            .then((album) => {
                if(!album){
                    return res.status(404).send({message: "Album does not exist!"});
                }
                return album   
                    .update({
                        name: req.body.name
                    })
                    .then((album) => res.status(200).send(album))
                    .catch((error) => res.status(400).send(error));
            })
            .catch((error) => res.status(400).send(error));
    },
    delete(req,res){
        return Album
            .findByPk(req.params.id,{

            })
            .then((album) => {
                if(!album){
                    return res.status(404).send({message: "Album does not exist!"});
                }
                return album   
                    .destroy()
                    .then((album) => res.status(200).send({
                        message: 'Album with id '+req.params.id+ ' deleted.'
                    }))
                    .catch((error) => res.status(400).send(error));
            })
            .catch((error) => res.status(400).send(error));
    },

    addWithTracks(req,res){
        return Album
            .create({
                name: req.body.name,
                artist_id: req.artistdetails.id,
                tracks: req.body.tracks
            },{
                include: [{
                    model: Track,
                    as: 'tracks'
                }]
            })
            .then((artist) => res.status(201).send(artist))
            .catch((error) => res.status(400).send(error));
    }

}
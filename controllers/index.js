const artist = require('./artist');
const album = require('./album');
const track = require('./track');
const role = require('./role');
const account = require('./account');
const playlist = require('./playlist');
const auth = require('./auth');
const user = require('./user');
const track_log = require('./track_log');
module.exports = {
    artist,
    album,
    track,
    role,
    account,
    playlist,
    auth,
    user,
    track_log
}

const track_log = require('../models').Track_log;
const User = require('../models').User;
module.exports = {

    log(req, res) {
        return track_log
            .create({
                track_id: req.params.id,
                user_id: req.userdetails.id
            })
            .then((log) => {
                return track_log
                    .findAll({
              
                        where: {
                            user_id: req.userdetails.id,
                            track_id: req.params.id
                        },
                        order: [ [ 'createdAt', 'DESC' ]],
                    })
                    .then((log) => {
                        if(log.length === 1){
                           return res.status(200).json({
                                "Times Played": log.length,
                                 "Last Played": 'First time'
                            })
                        }

                       return res.status(200).json({
                            
                            "Times Played": log.length,
                            "Last Played": log[1].createdAt
                        })

                    })
                    .catch((error) => res.status(400).send(error));
            })
            .catch((error) => res.status(400).send(error));

    }

}
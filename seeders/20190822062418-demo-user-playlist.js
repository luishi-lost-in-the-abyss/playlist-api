'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */

   return queryInterface.bulkInsert('Playlists', [
     
    {
    id: 1,
    name: 'demoplaylist101',
    createdAt: new Date(),
    updatedAt: new Date(),
    user_id: 1,
  },
  {
    id: 2,
    name: 'demoplaylist102',
    createdAt: new Date(),
    updatedAt: new Date(),
    user_id: 1,
  }

], {});

  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};

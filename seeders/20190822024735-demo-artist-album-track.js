'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */

   return queryInterface.bulkInsert('Tracks', [
     
    {
    id: 1,
    album_id: 1,
    name: 'demotrack101',
    duration: 999,
    createdAt: new Date(),
    updatedAt: new Date(),
    mp3file: 'fakepath'
  },{
    id: 2,
    album_id: 1,
    name: 'demotrack102',
    duration: 999,
    createdAt: new Date(),
    updatedAt: new Date(),
    mp3file: 'fakepath'
  },
  {
    id: 3,
    album_id: 2,
    name: 'demotrack103',
    duration: 999,
    createdAt: new Date(),
    updatedAt: new Date(),
    mp3file: 'fakepath'
  },{
    id: 4,
    album_id: 2,
    name: 'demotrack104',
    duration: 999,
    createdAt: new Date(),
    updatedAt: new Date(),
    mp3file: 'fakepath'
  }
,
  {
    id: 5,
    album_id: 2,
    name: 'demotrack105',
    duration: 999,
    createdAt: new Date(),
    updatedAt: new Date(),
    mp3file: 'fakepath'
  },{
    id: 6,
    album_id: 2,
    name: 'demotrack106',
    duration: 999,
    createdAt: new Date(),
    updatedAt: new Date(),
    mp3file: 'fakepath'
  },
  {
    id: 7,
    album_id: 3,
    name: 'demotrack107',
    duration: 999,
    createdAt: new Date(),
    updatedAt: new Date(),
    mp3file: 'fakepath'
  }

], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};

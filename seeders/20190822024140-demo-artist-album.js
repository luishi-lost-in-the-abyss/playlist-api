'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */


   return queryInterface.bulkInsert('Albums', [
     
    {
    id: 1,
    name: 'demoalbum101',
    artist_id: 1,
    createdAt: new Date(),
    updatedAt: new Date(),
  },{
    id: 2,
    name: 'demoalbum102',
    artist_id: 1,
    createdAt: new Date(),
    updatedAt: new Date(),
  },

  {
    id: 3,
    name: 'demoalbum103',
    artist_id: 1,
    createdAt: new Date(),
    updatedAt: new Date(),
  }



], {});

  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};

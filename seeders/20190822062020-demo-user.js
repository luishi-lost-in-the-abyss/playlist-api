'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});

    */

   return queryInterface.bulkInsert('Users', [
     
    {
    id: 1,
    name: 'demouser101',
    account_id: 2,
    createdAt: new Date(),
    updatedAt: new Date(),
    profileimg: null
  }

], {});
    

    
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};

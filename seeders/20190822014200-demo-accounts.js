'use strict';
const bcrypt = require('bcrypt');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */


   const hashedPassword = await new Promise((resolve, reject) => {
    bcrypt.hash('secret123', 5, function (err, hash) {
        if (err) reject(err)
        resolve(hash)
    });
})

    return queryInterface.bulkInsert('Accounts', [
     
    {
    id: 1,
    role_id: 4,
    username: 'demoartist101',
    password: hashedPassword,
    createdAt: new Date(),
    updatedAt: new Date(),
    isActive: true
  },{
    id: 2,
    role_id: 2,
    username: 'demouser101',
    password: hashedPassword,
    createdAt: new Date(),
    updatedAt: new Date(),
    isActive: true
  },{
    id: 3,
    role_id: 3,
    username: 'admin',
    password: hashedPassword,
    createdAt: new Date(),
    updatedAt: new Date(),
    isActive: true,
  },
  {
    id: 4,
    role_id: 3,
    username: 'admin2',
    password: hashedPassword,
    createdAt: new Date(),
    updatedAt: new Date(),
    isActive: false,
  }

], {});



  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};

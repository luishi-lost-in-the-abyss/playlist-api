'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
   return queryInterface.bulkInsert('Roles', [
     
    {
    id: 2,
    role_description: 'User',
    createdAt: new Date(),
    updatedAt: new Date()

  },{
    id: 3,
    role_description: 'Admin',
    createdAt: new Date(),
    updatedAt: new Date()
  },{
    id: 4,
    role_description: 'Artist',
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    id: 5,
    role_description: 'Demo Role',
    createdAt: new Date(),
    updatedAt: new Date()
  }

], {});


  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};

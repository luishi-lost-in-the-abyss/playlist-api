'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */

   return queryInterface.bulkInsert('user_playlists', [
     
    {
    id: 1,
    createdAt: new Date(),
    updatedAt: new Date(),
    track_id: 5,
    playlist_id: 1
  },
  {
    id: 2,
    createdAt: new Date(),
    updatedAt: new Date(),
    track_id: 6,
    playlist_id: 1
  },
  {
    id: 3,
    createdAt: new Date(),
    updatedAt: new Date(),
    track_id: 4,
    playlist_id: 2
  },
  

], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};

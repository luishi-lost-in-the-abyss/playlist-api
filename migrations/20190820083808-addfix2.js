'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
   return queryInterface.addColumn(
    'user_playlists', // name of Source model
    'playlist_id', // name of the key we're adding 
    {
      type: Sequelize.INTEGER,
      references: {
        model: 'Playlists', // name of Target model
        key: 'id', // key in Target model that we're referencing
      },
      onDelete: 'CASCADE',
    }
  );

  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  }
};

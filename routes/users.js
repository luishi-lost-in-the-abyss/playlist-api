var express = require('express');
var router = express.Router();


/* GET users listing. */
router.get('/' ,function(req, res, next) {

 res.status(200).send({
   message: 'Welcome to the Restful Api using Express and Postgres'
 })
});

module.exports = router;

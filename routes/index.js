var express = require('express');
var router = express.Router();

//Controllers
const artistController = require('../controllers').artist;
const albumController = require('../controllers').album;
const trackController = require('../controllers').track;
const roleController = require('../controllers').role;
const accountController = require('../controllers').account;
const authController = require('../controllers').auth;
const userController = require('../controllers').user;
const logController = require('../controllers').track_log;
/* GET home page. */

//middlewares
let middlewares = require('../middlewares');


router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});




//Artist Routes
router.get('/api/artist/album',middlewares.checkToken, middlewares.isArtist, artistController.getAlbums);
router.post('/api/artist/album',middlewares.checkToken, middlewares.isArtist, middlewares.uploadmp3.array('trackmp3',2), artistController.addAlbumWithTracks);
router.delete('/api/artist/album/:id',middlewares.checkToken, middlewares.isArtist, artistController.deleteAlbum);
router.put('/api/artist/album/:id',middlewares.checkToken, middlewares.isArtist, artistController.updateAlbumname);
router.get('/api/artist/album/:id',middlewares.checkToken, middlewares.isArtist, artistController.listTrackfromAlbum);
router.post('/api/artist/track/:albumid',middlewares.checkToken, middlewares.isArtist,middlewares.uploadmp3.array('trackmp3',2), artistController.addTrackstoAlbum);
router.delete('/api/artist/track/:id',middlewares.checkToken, middlewares.isArtist, artistController.deleteTrack);
router.get('/api/artist/profile',middlewares.checkToken, middlewares.isArtist, artistController.loadProfile);
router.put('/api/artist/profile',middlewares.checkToken, middlewares.isArtist, middlewares.uploadimg.single('profileImage'), artistController.editProfile);


/*
//Album Routes
router.get('/api/album', albumController.list);
router.get('/api/album/:id', albumController.getById);
router.get('/api/album/listbyartist/:id', albumController.listbyartist);
router.put('/api/album/:id', albumController.update);
router.post('/api/album', albumController.add);
router.delete('/api/album/:id', albumController.delete);
router.post('/api/album/addwithtracks', middlewares.checkToken, middlewares.isArtist, albumController.addWithTracks);

//Track Routes
router.get('/api/track/listfromalbum/:id', trackController.listfromalbum);
router.get('/api/track/listfromplaylist/:id', trackController.listfromplaylist);
router.get('/api/track', trackController.list);
router.get('/api/track/:id', trackController.getById);
router.put('/api/track/:id', trackController.update);
router.post('/api/track', trackController.add);
router.post('/api/track/addtoplaylist/:id', trackController.addtoplaylist);
router.delete('/api/track/removetoplaylist/:id', trackController.removetoplaylist);
router.delete('/api/track/:id', trackController.delete);
*/


//changepass
router.post('/account/changepass', middlewares.checkToken, accountController.changepass);


//Authentication Routes
router.post('/login',authController.login);
router.post('/register/artist', accountController.addartist);
router.post('/register/user', accountController.adduser);


//User Routes
router.get('/api/user/loadArtists',  middlewares.checkToken, middlewares.isUser, userController.loadArtists);
router.get('/api/user/loadAlbums/:artistid',  middlewares.checkToken, middlewares.isUser, userController.loadalbumsbyartist);
router.get('/api/user/loadAlbums',  middlewares.checkToken, middlewares.isUser, userController.loadAlbums);
router.get('/api/user/Tracks/:albumid',  middlewares.checkToken, middlewares.isUser, userController.loadtracksbyalbum);
router.get('/api/user/loadTracks',  middlewares.checkToken, middlewares.isUser, userController.loadTracks);
router.get('/api/user/playlist',  middlewares.checkToken, middlewares.isUser,userController.loaduserplaylists);
router.post('/api/user/playlist', middlewares.checkToken, middlewares.isUser, userController.createplaylist);
router.delete('/api/user/playlist/:id', middlewares.checkToken, middlewares.isUser, userController.deleteplaylist);
router.put('/api/user/playlist/:id', middlewares.checkToken, middlewares.isUser, userController.updateplaylist);
router.get('/api/user/loadtracksfromplaylist/:playlistid',  middlewares.checkToken, middlewares.isUser, userController.loadtracksfromplaylist);
router.post('/api/user/addtoplaylist/:playlistid', middlewares.checkToken, middlewares.isUser, userController.addtoplaylist);
router.delete('/api/user/removetoplaylist/:playlistid', middlewares.checkToken, middlewares.isUser, userController.removetoplaylist);
router.get('/api/user/profile',  middlewares.checkToken, middlewares.isUser, userController.loadProfile);
router.put('/api/user/profile',  middlewares.checkToken, middlewares.isUser, middlewares.uploadimg.single('profileImage'), userController.editProfile);


//log Routes
router.post('/api/track/log/:id', middlewares.checkToken, middlewares.isUser,logController.log);

//Admin Routes
  //Role Routes
router.get('/api/role', middlewares.checkToken, middlewares.isAdmin , roleController.list);
router.post('/api/role', middlewares.checkToken, middlewares.isAdmin ,roleController.add);
router.put('/api/role/:id', middlewares.checkToken, middlewares.isAdmin , roleController.update);
router.delete('/api/role/:id', middlewares.checkToken, middlewares.isAdmin , roleController.delete);
  //Account Routes
router.get('/api/account', middlewares.checkToken, middlewares.isAdmin , accountController.list);
router.post('/api/account/addadmin', middlewares.checkToken, middlewares.isAdmin , accountController.addadmin);
router.put('/api/account/ban/:id', middlewares.checkToken, middlewares.isAdmin , accountController.banAccount);
router.put('/api/account/unban/:id', middlewares.checkToken, middlewares.isAdmin , accountController.unbanAccount);
router.get('/api/artist', middlewares.checkToken, middlewares.isAdmin , accountController.listartistfull);
router.get('/api/album/:artistid',  middlewares.checkToken, middlewares.isAdmin, albumController.listbyartist);
router.delete('/api/album/:id', middlewares.checkToken, middlewares.isAdmin,albumController.delete);
router.get('/api/track/:albumid',middlewares.checkToken, middlewares.isAdmin, trackController.listfromalbum);
router.delete('/api/track/:id', middlewares.checkToken, middlewares.isAdmin,trackController.delete);

module.exports = router;

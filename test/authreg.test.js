var supertest = require('supertest');
var app = require('../app');



describe('TEST SUITE FOR AUTHENTICATION AND REGISTRATION',()=> {


    describe('POST /register/artist', () => {


        test('It should successfully create artist account and profile', async (done) => {
            const response = await supertest(app)
                .post('/register/artist')
                .send({
                    name: 'testingartist101',
                    username: 'testingartist101',
                    password: 'secret123'
                })
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(201);
            expect(response.body).toHaveProperty(
                'message',
                'Artist account created'
            );
            expect(response.body.account).toHaveProperty(
                'username',
                'testingartist101',
            );
            expect(response.body.account.artist).toHaveProperty(
                'name',
                'testingartist101',
            );
            done();
        });


         test('It should respond with an error if username is already taken', async (done) => {
            const response = await supertest(app)
                .post('/register/artist')
                .send({
                    name: 'testingartist101',
                    username: 'testingartist101',
                    password: 'secret123'
                })
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(409);
            expect(response.body).toHaveProperty(
                'message',
                'Username Taken'
            );
            done();
        });


    })
    
    
   


    describe('POST /register/user', () => {

        test('It should successfully create user account and profile', async (done) => {
            const response = await supertest(app)
                .post('/register/user')
                .send({
                    name: 'testinguser101',
                    username: 'testinguser101',
                    password: 'secret123'
                })
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(201);
            expect(response.body).toHaveProperty(
                'message',
                'User account created'
            );
            expect(response.body.account).toHaveProperty(
                'username',
                'testinguser101',
            );
            expect(response.body.account.user).toHaveProperty(
                'name',
                'testinguser101',
            );
            done();
        });

        test('It should respond with an error if username is already taken', async (done) => {
            const response = await supertest(app)
                .post('/register/user')
                .send({
                    name: 'testinguser101',
                    username: 'testinguser101',
                    password: 'secret123'
                })
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(409);
            expect(response.body).toHaveProperty(
                'message',
                'Username Taken'
            );
            done();
        });
    })
    
      

    describe('POST /login', () => {

        test('It should respond with a token if successful login', async (done) => {
            const response = await supertest(app)
                .post('/login')
                .send({
                    username: 'testinguser101',
                    password: 'secret123'
                })
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(200);
            expect(response.body).toHaveProperty(
                'message',
                'Authentication successful!'
            );
            expect(response.body).toHaveProperty(
                'token',
            );
            done();
        });

        test('It should respond with an error if username does not exist', async (done) => {
            const response = await supertest(app)
                .post('/login')
                .send({
                    username: 'qweqwe',
                    password: 'secret123'
                })
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(404);
            expect(response.body).toHaveProperty(
                'message',
                'Account Does not Exist!'
            );
            done();
        });
        
        test('It should respond with an error if Incorrect password', async (done) => {
            const response = await supertest(app)
                .post('/login')
                .send({
                    username: 'testinguser101',
                    password: 'secret1234'
                })
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(400);
            expect(response.body).toHaveProperty(
                'message',
                'Incorrect Password'
            );
            done();
        });

        test('It should respond with an error if account is banned', async (done) => {
            const response = await supertest(app)
                .post('/login')
                .send({
                    username: 'admin2',
                    password: 'secret123'
                })
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(403);
            expect(response.body).toHaveProperty(
                'message',
                'Account Banned!'
            );
            done();
        });

    })


   

})

var supertest = require('supertest');
var app = require('../app');


describe('TEST SUITE FOR USER ROUTES',()=> {
    const sampleimage = 'C:/Users/Default.DESKTOP-H07SCEJ/Downloads/paramore.jpg';
    const samplewrongimage = 'C:/Users/Default.DESKTOP-H07SCEJ/Downloads/test.txt';

    let loginData ={};
        beforeAll(async () => {
    
            loginData =  await supertest(app)
                    .post('/login')
                    .send({
                        username: 'demouser101',
                        password: 'secret123'
                    })
                    .set('Accept', 'application/json');
    
           });


    describe('GET /api/user/profile', () => {
        test('It should access its own profile', async (done) => {
            const response = await supertest(app)
                .get('/api/user/profile')
                .set('Authorization', 'Bearer '+loginData.body.token)
            expect(response.statusCode).toBe(200);
            expect(response.body).toHaveProperty(
                'user',        
            );
            done();
        });
    })


    describe('PUT /api/user/profile', () => {

        test('It should update its name and profile image', async (done) => {
            const response = await supertest(app)
                .put('/api/user/profile')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .field('name', 'demouser102')
                .attach('profileImage', sampleimage)
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(200);
            expect(response.body).toHaveProperty(
                'message',
                'Profile updated successfully.'        
            );
            expect(response.body.account.user).toHaveProperty(
                'name',
                'demouser102',   
            );
            done();
        });


        test('It should respond with an error if wrong file format is sent for image', async (done) => {
            console.log(loginData.body);
            const response = await supertest(app)
                .put('/api/user/profile')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .attach('profileImage', samplewrongimage)
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(415);
            expect(response.body).toHaveProperty(
                'message',
                'jpeg and png files are only accepted'        
            );
           
            done();
        });

    })   

    describe('GET /api/user/loadArtists', () => {
        test('It should list all artists', async (done) => {
            const response = await supertest(app)
                .get('/api/user/loadArtists')
                .set('Authorization', 'Bearer '+loginData.body.token)
            expect(response.statusCode).toBe(200);
            expect(response.body).toHaveProperty(
                'artists',        
            );
            done();
        });
    })

    describe('GET /api/user/loadAlbums', () => {
        test('It should list all albums', async (done) => {
            const response = await supertest(app)
                .get('/api/user/loadAlbums')
                .set('Authorization', 'Bearer '+loginData.body.token)
            expect(response.statusCode).toBe(200);
            expect(response.body).toHaveProperty(
                'albums',        
            );
            done();
        });
    })

    describe('GET /api/user/loadTracks', () => {
        test('It should list all tracks', async (done) => {
            const response = await supertest(app)
                .get('/api/user/loadTracks')
                .set('Authorization', 'Bearer '+loginData.body.token)
            expect(response.statusCode).toBe(200);
            expect(response.body).toHaveProperty(
                'tracks',        
            );
            done();
        });
    })


    describe('GET /api/user/loadAlbums/:artistid', () => {

        test('It should list all albums from an artist', async (done) => {
            const response = await supertest(app)
                .get('/api/user/loadAlbums/1')
                .set('Authorization', 'Bearer '+loginData.body.token)
            expect(response.statusCode).toBe(200);
            expect(response.body).toHaveProperty(
                'albums',        
            );
            done();
        });

        test('It should respond with an error if artist does not exist', async (done) => {
            const response = await supertest(app)
                .get('/api/user/loadAlbums/112323')
                .set('Authorization', 'Bearer '+loginData.body.token)
            expect(response.statusCode).toBe(404);
            expect(response.body).toHaveProperty(
                'message',
                'Artist does not exist'        
            );
            done();
        });
    })

    
    describe('GET /api/user/Tracks/:albumid', () => {

        test('It should list all tracks from an album', async (done) => {
            const response = await supertest(app)
                .get('/api/user/Tracks/2')
                .set('Authorization', 'Bearer '+loginData.body.token)
            expect(response.statusCode).toBe(200);
            expect(response.body).toHaveProperty(
                'tracks',        
            );
            done();
        });

        test('It should respond with an error if artist does not exist', async (done) => {
            const response = await supertest(app)
                .get('/api/user/Tracks/112323')
                .set('Authorization', 'Bearer '+loginData.body.token)
            expect(response.statusCode).toBe(404);
            expect(response.body).toHaveProperty(
                'message',
                'Album does not exist'        
            );
            done();
        });
    })

    describe('GET /api/user/playlist', () => {
        test('It should list all playlist of the user', async (done) => {
            const response = await supertest(app)
                .get('/api/user/playlist')
                .set('Authorization', 'Bearer '+loginData.body.token);
            expect(response.statusCode).toBe(200);
            expect(response.body).toHaveProperty(
                'playlists',        
            );
            done();
        });
    })

    describe('POST /api/user/playlist', ()=> {
        
        test('It should successfully create a user playlist', async (done) => {
            const response = await supertest(app)
                .post('/api/user/playlist')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .send({name: 'playlistnew'})
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(201);
            expect(response.body).toHaveProperty(
                'playlist',        
            );
            expect(response.body.playlist).toHaveProperty(
                'name',     
                'playlistnew'   
            );
            done();
        });

        test('It should respond with an error if playlist name is already been used', async (done) => {
            const response = await supertest(app)
                .post('/api/user/playlist')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .send({name: 'playlistnew'})
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(409);
            expect(response.body).toHaveProperty(
                'message',
                'Playlist Name Already Exist'        
            );
           
            done();
        });
    })


    describe('PUT /api/user/playlist/:id',()=> {

        test('It should successfully rename a playlist', async (done) => {
            const response = await supertest(app)
                .put('/api/user/playlist/1')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .send({name: 'demoplaylist101 updated'})
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(200);
            expect(response.body).toHaveProperty(
                'message',
                'Updated Sucessfully'        
            );
            done();
        });

        test('It should respond with an error if playlist name is already taken', async (done) => {
            const response = await supertest(app)
                .put('/api/user/playlist/1')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .send({name: 'playlistnew'})
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(409);
            expect(response.body).toHaveProperty(
                'message',
                'Playlist Name Already Exist'        
            );
            done();
        });

        test('It should respond with an error if renaming an album that doesnt exist', async (done) => {
            const response = await supertest(app)
                .put('/api/user/playlist/201')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .send({name: 'demoalbum101qweqwe updated'})
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(404);
            expect(response.body).toHaveProperty(
                'message',
                'Playlist does not exist!'        
            );
            done();
        });
        
    })


    describe('POST /api/user/addtoplaylist/:playlistid', () => {

        test('It should successfully add a track to a playlist', async (done) => {
            const response = await supertest(app)
                .post('/api/user/addtoplaylist/1')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .send({track_id: '6'})
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(200);
            done();
        });

        test('It should respond an error if playlist does not exist', async (done) => {
            const response = await supertest(app)
                .post('/api/user/addtoplaylist/100')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .send({track_id: '6'})
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(404);
            expect(response.body).toHaveProperty(
                'message',
                'Playlist Not Found'        
            );
            done();
        });

        test('It should respond an error if playlist does not exist', async (done) => {
            const response = await supertest(app)
                .post('/api/user/addtoplaylist/1')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .send({track_id: '600'})
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(404);
            expect(response.body).toHaveProperty(
                'message',
                'Track Not Found'        
            );
            done();
        });

    })

    describe('DELETE /api/user/removetoplaylist/:playlistid', () => {

        test('It should successfully remove a track to a playlist', async (done) => {
            const response = await supertest(app)
                .delete('/api/user/removetoplaylist/1')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .send({track_id: '6'})
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(200);
            done();
        });

        test('It should respond an error if playlist does not exist', async (done) => {
            const response = await supertest(app)
                .delete('/api/user/removetoplaylist/100')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .send({track_id: '6'})
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(404);
            expect(response.body).toHaveProperty(
                'message',
                'Playlist Not Found'        
            );
            done();
        });

        test('It should respond an error if playlist does not exist', async (done) => {
            const response = await supertest(app)
                .delete('/api/user/removetoplaylist/1')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .send({track_id: '600'})
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(404);
            expect(response.body).toHaveProperty(
                'message',
                'Track Not Found'        
            );
            done();
        });

    })


    describe('GET /api/user/loadtracksfromplaylist/:playlistid', () => {

        test('It should list all tracks from a playlist', async (done) => {
            const response = await supertest(app)
                .get('/api/user/loadtracksfromplaylist/1')
                .set('Authorization', 'Bearer '+loginData.body.token);
            expect(response.statusCode).toBe(200);
            done();
        });


        test('It should respond with an error if playlist does not exist', async (done) => {
            const response = await supertest(app)
                .get('/api/user/loadtracksfromplaylist/100')
                .set('Authorization', 'Bearer '+loginData.body.token);
            expect(response.statusCode).toBe(404);
            expect(response.body).toHaveProperty(
                'message',
                'Playlist Not Found'        
            );
            done();
        });

    })

    describe('DELETE /api/user/playlist/:id', () => {

        test('It should successfully delete a playlist', async (done) => {
            const response = await supertest(app)
                .delete('/api/user/playlist/1')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(200);
            expect(response.body).toHaveProperty(
                'message',
                'Playlist with id 1 deleted.'        
            );
            done();
        });


        test('It should respond with an error if playlist doesnt exist', async (done) => {
            const response = await supertest(app)
                .delete('/api/user/playlist/100')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(404);
            expect(response.body).toHaveProperty(
                'message',
                'Playlist Does not Exist!'        
            );
            done();
        });
    });
   

    describe('POST /api/track/log/:id', () => {

        test('It should log track', async (done) => {
            const response = await supertest(app)
                .post('/api/track/log/6')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(200);
            expect(response.body).toHaveProperty(
                'Times Played',       
            );
            expect(response.body).toHaveProperty(
                'Last Played',       
            );
            done();
        });
        
    })




})

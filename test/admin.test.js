var supertest = require('supertest');
var app = require('../app');
const Role = require('../models').Role;


describe('TEST SUITE FOR ADMIN ROUTES', () => {


    let loginData ={};
        beforeAll(async () => {
    
            loginData =  await supertest(app)
                    .post('/login')
                    .send({
                        username: 'admin',
                        password: 'secret123'
                    })
                    .set('Accept', 'application/json');
    
           });


           afterAll(async () => {
        await Role.destroy({ truncate : true, cascade: true });
    
           });

    describe('GET /api/role', () => {

        test('It should list all roles', async (done) => {
            const response = await supertest(app)
                .get('/api/role')
                .set('Authorization', 'Bearer '+loginData.body.token)
            expect(response.statusCode).toBe(200);
            done();
        });

    })

    describe('POST /api/role', () => {

        test('It should successfully add a new role', async (done) => {
            const response = await supertest(app)
                .post('/api/role')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .send({role_description: 'testingpurposes'})
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(201);
            expect(response.body).toHaveProperty(
                'role_description',     
                'testingpurposes'   
            );
            done();
        });

        test('It should respond with an error if desc is alrady taken', async (done) => {
            const response = await supertest(app)
                .post('/api/role')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .send({role_description: 'testingpurposes'})
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(409);
            expect(response.body).toHaveProperty(
                'message',     
                'Role already exist'   
            );
            done();
        });

    })

    describe('PUT /api/role/:id', () => {
        test('It should successfully update a role description', async (done) => {
            const response = await supertest(app)
                .put('/api/role/2')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .send({role_description: 'User updated'})
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(200);
            expect(response.body).toHaveProperty(
                'role_description',     
                'User updated'   
            );
            done();
        });


        test('It should respond an error if desc is already taken', async (done) => {
            const response = await supertest(app)
                .put('/api/role/2')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .send({role_description: 'User updated'})
                .set('Accept', 'application/json');
                expect(response.statusCode).toBe(409);
                expect(response.body).toHaveProperty(
                    'message',     
                    'Role already exist'   
                );
            done();
        });

        test('It should respond an error if role does not exist', async (done) => {
            const response = await supertest(app)
                .put('/api/role/200')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .send({role_description: 'User updatedqwe'})
                .set('Accept', 'application/json');
                expect(response.statusCode).toBe(404);
                expect(response.body).toHaveProperty(
                    'message',     
                    'Role does not exist!'   
                );
            done();
        });
    })


    describe('DELETE /api/role/:id', () => {

        test('It should successfully delete a role', async (done) => {
            const response = await supertest(app)
                .delete('/api/role/5')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .set('Accept', 'application/json');
                expect(response.statusCode).toBe(200);
            done();
        });

        test('It should respond an error if role doesnt exist', async (done) => {
            const response = await supertest(app)
                .delete('/api/role/200')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .set('Accept', 'application/json');
                expect(response.statusCode).toBe(404);
                expect(response.body).toHaveProperty(
                    'message',     
                    'Role does not exist!'   
                );

            done();
        });
    })

    describe('GET /api/account', () => {
        test('It should list all accounts', async (done) => {
            const response = await supertest(app)
                .get('/api/account')
                .set('Authorization', 'Bearer '+loginData.body.token)
            expect(response.statusCode).toBe(200);
            expect(response.body).toHaveProperty(
                'accounts',     
             
            );
            done();
        });
    })

    describe('/api/account/addadmin', ()=>{

        test('It should successfully create a new admin', async (done) => {
                    const response = await supertest(app)
                        .post('/api/account/addadmin')
                        .set('Authorization', 'Bearer '+loginData.body.token)
                        .send({username: 'newadmin', password: 'secret123'})
                        .set('Accept', 'application/json');
                    expect(response.statusCode).toBe(201);
                    expect(response.body).toHaveProperty(
                        'message',     
                        'Account created'   
                    );
                    expect(response.body.account).toHaveProperty(
                        'username',     
                        'newadmin'   
                    );
                    done();
                });

                test('It should respond with an error if username is taken', async (done) => {
                    const response = await supertest(app)
                        .post('/api/account/addadmin')
                        .set('Authorization', 'Bearer '+loginData.body.token)
                        .send({username: 'newadmin', password: 'secret123'})
                        .set('Accept', 'application/json');
                    expect(response.statusCode).toBe(409);
                    expect(response.body).toHaveProperty(
                        'message',     
                        'Username Taken'   
                    );

                    done();
                });

    })

    describe('PUT /api/account/ban/:id', () => {

        test('It should change the status of an account to inactive', async (done) => {
            const response = await supertest(app)
                .put('/api/account/ban/1')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(200);
            expect(response.body.account).toHaveProperty(
                'isActive',     
                false
            );
            done();
        });

        test('It should respond with an error if account does not exist', async (done) => {
            const response = await supertest(app)
                .put('/api/account/ban/100')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(404);
            expect(response.body).toHaveProperty(
                'message',     
                'Account does not exist!'
            );
            done();
        });
    })

    describe('PUT /api/account/unban/:id', () => {

        test('It should change the status of an account to active', async (done) => {
            const response = await supertest(app)
                .put('/api/account/unban/1')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(200);
            expect(response.body.account).toHaveProperty(
                'isActive',     
                true
            );
            done();
        });

        test('It should respond with an error if account does not exist', async (done) => {
            const response = await supertest(app)
                .put('/api/account/unban/100')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(404);
            expect(response.body).toHaveProperty(
                'message',     
                'Account does not exist!'
            );
            done();
        });
    })

    describe('GET /api/artist', () => {
        test('It should list all artist and their details', async (done) => {
            const response = await supertest(app)
                .get('/api/artist')
                .set('Authorization', 'Bearer '+loginData.body.token)
            expect(response.statusCode).toBe(200);
            done();
        });
    })

    describe('GET /api/album/:artistid', () => {
        test('It should list all albums of an artist', async (done) => {
            const response = await supertest(app)
                .get('/api/album/1')
                .set('Authorization', 'Bearer '+loginData.body.token)
            expect(response.statusCode).toBe(200);
            done();
        });

        test('It should respond and error if an artist does not exist', async (done) => {
            const response = await supertest(app)
                .get('/api/album/100')
                .set('Authorization', 'Bearer '+loginData.body.token)
            expect(response.statusCode).toBe(404);
            expect(response.body).toHaveProperty(
                'message',     
                'Artist does not exist'
            );
            done();
        });
    })

    describe('DELETE /api/album/:id', ()=>{
        test('It should successfully delete an album', async (done) => {
            const response = await supertest(app)
                .delete('/api/album/2')
                .set('Authorization', 'Bearer '+loginData.body.token)
            expect(response.statusCode).toBe(200);
            done();
        });

        test('It should respond an error if album does not exist', async (done) => {
            const response = await supertest(app)
                .delete('/api/album/200')
                .set('Authorization', 'Bearer '+loginData.body.token)
            expect(response.statusCode).toBe(404);
            expect(response.body).toHaveProperty(
                'message',     
                'Album does not exist!'
            );
            done();
        });
    })

    describe('GET /api/track/:albumid', () => {
        test('It should successfully list track from an album', async (done) => {
            const response = await supertest(app)
                .get('/api/track/3')
                .set('Authorization', 'Bearer '+loginData.body.token)
            expect(response.statusCode).toBe(200);
            done();
        });

        test('It should respond an error if album does not exist', async (done) => {
            const response = await supertest(app)
                .get('/api/track/300')
                .set('Authorization', 'Bearer '+loginData.body.token)
            expect(response.statusCode).toBe(404);
            expect(response.body).toHaveProperty(
                'message',     
                'Album does not exist'
            );
            done();
        });
    })


    describe('DELETE /api/track/:id', () => {
        test('It should successfully delete a track', async (done) => {
            const response = await supertest(app)
                .delete('/api/track/7')
                .set('Authorization', 'Bearer '+loginData.body.token)
            expect(response.statusCode).toBe(200);
            done();
        });

        test('It should respond an error if album does not exist', async (done) => {
            const response = await supertest(app)
                .delete('/api/track/300')
                .set('Authorization', 'Bearer '+loginData.body.token)
            expect(response.statusCode).toBe(404);
            expect(response.body).toHaveProperty(
                'message',     
                'track does not exist!'
            );
            done();
        });
    })

    describe('POST /account/changepass', () => {

        test('It should change the password succesfully', async (done) => {
            const response = await supertest(app)
                .post('/account/changepass')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .send({oldpassword: 'secret123', newpassword: 'secret123456'})
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(200);
            expect(response.body).toHaveProperty(
                'message',     
                'Password changed.'
            );
            done();
        });

        test('It should respond an error if wrong old password is sent', async (done) => {
            const response = await supertest(app)
                .post('/account/changepass')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .send({oldpassword: 'secret123', newpassword: 'cant_reached'})
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(400);
            expect(response.body).toHaveProperty(
                'message',     
                'Incorrect Password!'
            );
            done();
        });

    })

})
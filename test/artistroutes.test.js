var supertest = require('supertest');
var app = require('../app');


describe('TEST SUITE FOR ARTIST ROUTES',()=> {
    const sampleimage = 'C:/Users/Default.DESKTOP-H07SCEJ/Downloads/paramore.jpg';
    const samplewrongimage = 'C:/Users/Default.DESKTOP-H07SCEJ/Downloads/test.txt';
    const samplemp3 = 'C:/Users/Default.DESKTOP-H07SCEJ/Downloads/Ignorance.mp3';

    let loginData ={};
        beforeAll(async () => {
    
            loginData =  await supertest(app)
                    .post('/login')
                    .send({
                        username: 'demoartist101',
                        password: 'secret123'
                    })
                    .set('Accept', 'application/json');
    
           });


    describe('GET /api/artist/profile', () => {
        test('It should access its own profile', async (done) => {
            const response = await supertest(app)
                .get('/api/artist/profile')
                .set('Authorization', 'Bearer '+loginData.body.token)
            expect(response.statusCode).toBe(200);
            expect(response.body).toHaveProperty(
                'artist',        
            );
            done();
        });
    })


    describe('PUT /api/artist/profile', () => {

        test('It should update its name and profile image', async (done) => {
            const response = await supertest(app)
                .put('/api/artist/profile')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .field('name', 'demoartist102')
                .attach('profileImage', sampleimage)
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(200);
            expect(response.body).toHaveProperty(
                'message',
                'Profile updated successfully.'        
            );
            expect(response.body.account.artist).toHaveProperty(
                'name',
                'demoartist102',   
            );
            done();
        });


        test('It should respond with an error if wrong file format is sent for image', async (done) => {
            console.log(loginData.body);
            const response = await supertest(app)
                .put('/api/artist/profile')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .attach('profileImage', samplewrongimage)
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(415);
            expect(response.body).toHaveProperty(
                'message',
                'jpeg and png files are only accepted'        
            );
           
            done();
        });

    })


    describe('GET /api/artist/album',() => {
        test('It should successfully load all albums', async (done) => {
            const response = await supertest(app)
                .get('/api/artist/album')
                .set('Authorization', 'Bearer '+loginData.body.token);
            expect(response.statusCode).toBe(200);
            expect(response.body).toHaveProperty(
                'albums',
            );
            done();
        });
    });


    describe('POST /api/artist/album', () => {
        test('It should successfully create an album with tracks in it', async (done) => {
            const response = await supertest(app)
                .post('/api/artist/album')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .field('name', 'demoartist102_album1')
                .attach('trackmp3', samplemp3)
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(201);
            expect(response.body).toHaveProperty(
                'message',
                'Album demoartist102_album1 created'        
            );
            done();
        });

        test('It should respond with an error if album name already exist', async (done) => {
            const response = await supertest(app)
                .post('/api/artist/album')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .field('name', 'demoartist102_album1')
                .attach('trackmp3', samplemp3)
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(409);
            expect(response.body).toHaveProperty(
                'message',
                'Album Name Already Exist'        
            );
            done();
        });


        test('It should respond with an error if no track is attached', async (done) => {
            const response = await supertest(app)
                .post('/api/artist/album')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .field('name', 'demoartist102qweqwe_album1')
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(400);
            expect(response.body).toHaveProperty(
                'message',
                '\"tracks\" must contain at least 1 items'        
            );
            done();
        });

        test('It should respond with an error if other filetype is attached', async (done) => {
            const response = await supertest(app)
                .post('/api/artist/album')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .field('name', 'demoartist102qweqwe_album1')
                .attach('trackmp3', sampleimage)
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(415);
            expect(response.body).toHaveProperty(
                'message',
                'mp3 files are only accepted'        
            );
            done();
        });


    })

    describe('PUT /api/artist/album/:id', () => {

        test('It should successfully rename an album', async (done) => {
            const response = await supertest(app)
                .put('/api/artist/album/1')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .send({name: 'demoalbum101 updated'})
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(200);
            expect(response.body).toHaveProperty(
                'message',
                'Updated Sucessfully'        
            );
            done();
        });

        test('It should respond with an error if album name is already taken', async (done) => {
            const response = await supertest(app)
                .put('/api/artist/album/1')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .send({name: 'demoalbum101 updated'})
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(409);
            expect(response.body).toHaveProperty(
                'message',
                'Album Name Already Exist'        
            );
            done();
        });

        test('It should respond with an error if renaming an album that doesnt exist', async (done) => {
            const response = await supertest(app)
                .put('/api/artist/album/101000')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .send({name: 'demoalbum101qweqwe updated'})
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(404);
            expect(response.body).toHaveProperty(
                'message',
                'Album does not exist!'        
            );
            done();
        });
        

    })

    describe('DELETE /api/artist/album/:id',()=>{

        test('It should successfully delete an album', async (done) => {
            const response = await supertest(app)
                .delete('/api/artist/album/1')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(200);
            expect(response.body).toHaveProperty(
                'message',
                'Album with id 1 deleted.'        
            );
            done();
        });

        test('It should respond with an error if album does not exist', async (done) => {
            const response = await supertest(app)
                .delete('/api/artist/album/10100')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(404);
            expect(response.body).toHaveProperty(
                'message',
                'Album does not exist!'        
            );
            done();
        });
    })

    describe('GET /api/artist/album/:id', () => {

        test('It should successfully list track from an album', async (done) => {
            const response = await supertest(app)
                .get('/api/artist/album/2')
                .set('Authorization', 'Bearer '+loginData.body.token);
            expect(response.statusCode).toBe(200);
            done();
        });

        test('It should respond with an error if album does not exist', async (done) => {
            const response = await supertest(app)
                .get('/api/artist/album/10100')
                .set('Authorization', 'Bearer '+loginData.body.token);
            expect(response.statusCode).toBe(404);
            expect(response.body).toHaveProperty(
                'message',
                'Album does not exist'
            );
            done();
        });

    })

    describe('POST /api/artist/track/:albumid', () => {
        test('It should successfully add tracks to an album', async (done) => {
            const response = await supertest(app)
                .post('/api/artist/track/2')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .attach('trackmp3', samplemp3)
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(201);
            expect(response.body).toHaveProperty(
                'message',
                'Tracks added to album demoalbum102'        
            );
            done();
        });

        test('It should respond with an error if album doesnt exist', async (done) => {
            const response = await supertest(app)
                .post('/api/artist/track/10100')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .attach('trackmp3', samplemp3)
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(404);
            expect(response.body).toHaveProperty(
                'message',
                'Album Does not exist'        
            );
            done();
        });

        test('It should respond with an error if there is an invalid filetype', async (done) => {
            const response = await supertest(app)
                .post('/api/artist/track/2')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .attach('trackmp3', sampleimage)
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(415);
            expect(response.body).toHaveProperty(
                'message',
                'mp3 files are only accepted'        
            );
            done();
        });
    })

    describe('DELETE /api/artist/track/:id', () => {
        
        test('It should successfully delete a track', async (done) => {
            const response = await supertest(app)
                .delete('/api/artist/track/3')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(200);
            expect(response.body).toHaveProperty(
                'message',
                'Track with id 3 deleted.'        
            );
            done();
        });


        test('It should respond with an error if track doesnt exist', async (done) => {
            const response = await supertest(app)
                .delete('/api/artist/track/3')
                .set('Authorization', 'Bearer '+loginData.body.token)
                .set('Accept', 'application/json');
            expect(response.statusCode).toBe(404);
            expect(response.body).toHaveProperty(
                'message',
                'track does not exist!'        
            );
            done();
        });
    });
    
})

const first = require('./authreg.test');
const second = require('./artistroutes.test');
const third = require('./userroutes.test');
const fourth = require('./admin.test');
module.exports = {
    first,
    second,
    third,
    fourth
}